var path = {
	sourceImages : "./static/images/**",
	destImages   : "./../public/static/images/",

	sourceLibsJs : './js/libs/**/*.*',
	destLibsJs   : './../public/js/libs/',
};

module.exports = path;
