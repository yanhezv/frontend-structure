var gulp   = require('gulp'),
	path   = require('./path'),
	uglify = require('gulp-uglify');
	
gulp.task('copy:images', ['clean:images'], function(){
	return gulp
	.src(path.sourceImages)
	.pipe(gulp.dest(path.destImages));
});

gulp.task('copy:libsJs', ['clean:libsJs'] , function(){
	return gulp
	.src(path.sourceLibsJs)
	.pipe(gulp.dest(path.destLibsJs));
});
