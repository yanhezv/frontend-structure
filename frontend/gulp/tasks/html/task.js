var gulp        = require('gulp'),
    config      = require('./config'),
    fn          = require('./functions'),
    path        = require('./path'),

    pug         = require('gulp-pug'),
    pugLint     = require('gulp-pug-lint'),
    pugNative   = require('pug'),
    read        = require('read-file');
    
function compileHtml(views, isWatch){
   config.pug    = fn.pugAdapter(pugNative);
   config.locals = {fs_read : read};

   return gulp.src(views, {base : path.pug})
   .pipe(pugLint())
   .pipe(pug(config))
   .pipe(gulp.dest(path.dest))
   .on('end', function() {
      if (isWatch) {
         console.log("Compiled: ",views);
      }
   });
}

gulp.task('html', ['clean:html'], function(p){
  return compileHtml(path.source);
});

gulp.task('html:watch', function () {
  gulp.watch(path.sourceWatch).on('change', function(file) {
    var pathChange = file.path.replace(/.+(?:pug\/)/, "");
    
    if (/^modules\//.test(pathChange)) {
      return compileHtml(file.path, true);
    }
    else{
      return fn.getViewToCompile(function(obj){
        return compileHtml(obj[pathChange], true);
      });
    }
    
  });
});
