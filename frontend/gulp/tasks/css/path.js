var path = {
	base        : './sass',
	source      : './sass/modules/*.sass',
	dest        : './../public/css/',
	
	sourceWatch : './sass/**/*.{sass,scss}',
};

module.exports = path;
