var gulp        = require('gulp'),
	path         = require('./path'),

	autoprefixer = require('gulp-autoprefixer'),
	sass         = require('gulp-sass'),
	sassGlob     = require('gulp-sass-glob'),
	preprocess   = require('gulp-preprocess');

gulp.task('css', ['clean:css'], function () {
	return gulp
	.src(path.source)
	.pipe(sassGlob())
	.pipe(sass({
		includePaths: [path.base]
	})
	.on('error', sass.logError))
	.pipe(preprocess({
		context: {color: 'green'}
	}))
   .pipe(autoprefixer())
	.pipe(gulp.dest(path.dest));
});

gulp.task('css:watch', function () {
	gulp.watch(path.sourceWatch, ['css']);
});
